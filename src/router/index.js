import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('../views/Dashboard.vue')
  },
  {
    path: '/clientes',
    name: 'customers',
    component: () => import('../views/Customers.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
